# Hochschule Darmstadt - Fortgeschrittene Webentwicklung

Mehr Informationen zum Kurs:
https://www.fbi.h-da.de/organisation/personen/mueller-david.html

## Spielregeln zum Praxis-Projekt

Das Praxisprojekt wird in Gruppen durchgeführt! Erstellt werden soll eine moderne Webanwendung, die den in der Veranstaltung vorgestellten, **hohen Standards** genügt. Die Wahl von Sprache, Framework etc. sind frei, es werden aber Empfehlungen und **einzuhaltende Kriterien** ausgesprochen.

### git rocks so good